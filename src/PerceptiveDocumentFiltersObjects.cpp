/*
Copyright (c) 1988-2013, Perceptive Sofware. All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/****************************************************************************
* Perceptive Document Filters 11 C++ objects & helper functions
****************************************************************************/

#include "PerceptiveDocumentFiltersObjects.h"

#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
#include <io.h>     // setmode()
#include <fcntl.h>  // O_BINARY
#include <direct.h> // _mkdir()
#include <stdio.h>  // _wfopen_s
#pragma warning(disable: 4996)
#else
#include <unistd.h> // usleep()
#endif

namespace Perceptive
{
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
const char PathDelim = '\\';
#else
const char PathDelim = '/';
#endif

std::wstring ToWideStr(const WCHAR *S, const int NumChars)
{
  std::wstring Result = L"";
  Result.reserve(NumChars);
  for (int i = 0; i < NumChars; i++)
  {
    wchar_t Ch = S[i];
    Result.append(1, Ch);
  }
  return Result;
}

std::string ToUTF8Str(const WCHAR *S, const int NumChars)
{
  std::string Result = "";
  int BufferLen = 1;
  if (NumChars < 0)
    for (int i = 0; S[i]; i++)
      BufferLen++;
  else
    BufferLen = NumChars + 1;
  LPSTR Buffer = (LPSTR)malloc(BufferLen * 4);
  if (Buffer)
  {
    if (NumChars < 0)
      Widechar_to_UTF8(S, Buffer, BufferLen * 4);
    else
      Widechar_to_UTF8_Ex(S, NumChars, Buffer, BufferLen * 4);
    Result.assign((char *)Buffer);
    free(Buffer);
  }
  return Result;
}

std::string ToWCHARStr(const std::string &S)
{
  std::string Result = "";
  int BufferLen = int(S.length()) + 1;
  LPWSTR Buffer = (LPWSTR)malloc(BufferLen * sizeof(WCHAR));
  if (Buffer)
  {
    UTF8_to_Widechar_Ex(S.c_str(), (LONG)S.size(), Buffer, BufferLen);
    Result.assign((char *)Buffer, BufferLen * sizeof(WCHAR));
    free(Buffer);
  }
  return Result;
}

std::string ToWCHARStr(const std::wstring &S)
{
  std::string Result = "";
  int BufferLen = int(S.length()) + 1;
  LPWSTR Buffer = (LPWSTR)malloc(BufferLen * sizeof(WCHAR));
  if (Buffer)
  {
    int i = 0;
    while (i < BufferLen)
    {
      Buffer[i] = S[i];
      i++;
    }
    Buffer[i] = (WCHAR)NULL;
    Result.assign((char *)Buffer, BufferLen * sizeof(WCHAR));
    free(Buffer);
  }
  return Result;
}

/****************************************************************************
* Perceptive::Stream
****************************************************************************/

Stream::Stream()
{
  FStream = NULL;
}

Stream::Stream(IGR_Stream *Stream)
{
  FStream = Stream;
}

Stream::~Stream()
{
  Close();
}

void Stream::Close()
{
  if (FStream)
  {
    FStream->Close(FStream);
    FStream = NULL;
  }
}

FileStream::FileStream(const std::string &Filename)
{
  Close();
  ECB_Type ECBError;
  LONG RC = IGR_Make_Stream_From_File(_UCS2(Filename), 0, &FStream, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Make_Stream_From_File: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

MemStream::MemStream(void *Data, LONG DataSize)
{
  Close();
  ECB_Type ECBError;
  LONG RC = IGR_Make_Stream_From_Memory(Data, DataSize, NULL, &FStream, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Make_Stream_From_Memory: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

/****************************************************************************
* Perceptive::Extractor
****************************************************************************/

Extractor::Extractor(Stream *S)
{
  FTimer.start();
  FStream = S;
  FCapabilities = 0;
  FDocType = 0;
  FDocHandle = 0;
  FTextEOF = false;
  FNumChars = 0;
  FPageIndex = 0;
}

Extractor::~Extractor()
{
  Close();
  FStream->Close();
  delete FStream;
  FTimer.stop();
}

void Extractor::Open(const int Flags, const std::string &Options)
{
  if (FDocHandle == 0)
  {
    ECB_Type ECBError;
    LONG RC = IGR_Open_Stream_Ex(FStream->getIGRStream(), Flags, _UCS2(Options), &FCapabilities, &FDocType, &FDocHandle, &ECBError);
    if (RC != IGR_OK)
    {
      std::string Err = "IGR_Open_Stream_Ex: " + GetErrorStr(RC);
      if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
      throw Exception(Err);
    }
  }
}

int Extractor::getFileType()
{
  if (FDocType == 0)
  {
    ECB_Type ECBError;
    LONG RC = IGR_Get_Stream_Type(FStream->getIGRStream(), &FCapabilities, &FDocType, &ECBError);
    if (RC != IGR_OK)
    {
      FCapabilities = 0;
      FDocType = 0;
    }
  }
  return FDocType;
}

std::string Extractor::getFileType(int What)
{
  (void)getFileType();
  const int BufferLen = 256;
  std::string Result = "Unknown";

  ECB_Type ECBError;
  LPSTR Buffer = (LPSTR)malloc(BufferLen);
  if (Buffer)
  {
    LONG RC = IGR_Get_Format_Attribute(FDocType, What, Buffer, &ECBError);
    if (RC == IGR_OK)
      Result.assign((char *)Buffer);
    free(Buffer);
  }
  return Result;
}

bool Extractor::getSupportsText()
{
  (void)getFileType();
  return (FCapabilities & IGR_FILE_SUPPORTS_TEXT) > 0;
}

bool Extractor::getSupportsSubFiles()
{
  (void)getFileType();
  return (FCapabilities & IGR_FILE_SUPPORTS_SUBFILES) > 0;
}

bool Extractor::getEOF()
{
  return FTextEOF;
}

std::string Extractor::GetText(const int MaxLength)
{
  Open();
  std::string Result = "";
  ECB_Type ECBError;
  LONG OutBuffSize = MaxLength;
  WCHAR *OutBuffer = (WCHAR *)malloc(MaxLength * sizeof(WCHAR));
  if (OutBuffer)
  {
    LONG RC = IGR_Get_Text(FDocHandle, OutBuffer, &OutBuffSize, &ECBError);
    if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
    {
      free(OutBuffer);
      std::string Err = "IGR_Get_Text: " + GetErrorStr(RC);
      if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
      throw Exception(Err);
    }
    FTextEOF = (RC == IGR_NO_MORE);
    if (OutBuffSize > 0)
    {
      Result = ToUTF8Str(OutBuffer, OutBuffSize);
      FNumChars += OutBuffSize;
    }
    free(OutBuffer);
  }
  return Result;
}

std::wstring Extractor::GetTextW(const int MaxLength)
{
  Open();
  std::wstring Result = L"";
  ECB_Type ECBError;
  LONG OutBuffSize = MaxLength;
  WCHAR *OutBuffer = (WCHAR *)malloc(MaxLength * sizeof(WCHAR));
  if (OutBuffer)
  {
    LONG RC = IGR_Get_Text(FDocHandle, OutBuffer, &OutBuffSize, &ECBError);
    if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
    {
      free(OutBuffer);
      std::string Err = "IGR_Get_Text: " + GetErrorStr(RC);
      if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
      throw Exception(Err);
    }
    FTextEOF = (RC == IGR_NO_MORE);
    if (OutBuffSize > 0)
    {
      Result = ToWideStr(OutBuffer, OutBuffSize);
      FNumChars += OutBuffSize;
    }
    free(OutBuffer);
  }
  return Result;
}

std::string Extractor::PrepareText(const std::string &S)
{
  std::string Result;
  Result.reserve(S.length() * 2);
  for (size_t i = 0; i < S.length(); i++)
  {
    char Ch = S[i];
    switch (Ch)
    {
    case IGR_CHAR_SOFT_SPACE:
      Result.append(" ");
      break;
    case IGR_CHAR_STYLE_NORMAL:
    case IGR_CHAR_STYLE_BOLD:
    case IGR_CHAR_STYLE_ITALICS:
    case IGR_CHAR_STYLE_UNDERLINED:
    case IGR_CHAR_START_META:
    case IGR_CHAR_END_META:
      break;
    case IGR_CHAR_TAB:
      Result.append("  ");
      break;
    case IGR_CHAR_LINE_BREAK:
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
      Result.append("\r\n");
#else
      Result.append("\n");
#endif
      break;
    case IGR_CHAR_PAGE_BREAK:
    case IGR_CHAR_PARA_BREAK:
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
      Result.append("\r\n\r\n");
#else
      Result.append("\n\n");
#endif
      break;
    default:
      Result.append(1, Ch);
      break;
    }
  }
  return Result;
}

std::wstring Extractor::PrepareTextW(const std::wstring &S)
{
  std::wstring Result;
  Result.reserve(S.length() * 2);
  for (size_t i = 0; i < S.length(); i++)
  {
    wchar_t Ch = S[i];
    switch (Ch)
    {
    case IGR_CHAR_SOFT_SPACE:
      Result.append(L" ");
      break;
    case IGR_CHAR_STYLE_NORMAL:
    case IGR_CHAR_STYLE_BOLD:
    case IGR_CHAR_STYLE_ITALICS:
    case IGR_CHAR_STYLE_UNDERLINED:
    case IGR_CHAR_START_META:
    case IGR_CHAR_END_META:
      break;
    case IGR_CHAR_TAB:
      Result.append(L"  ");
      break;
    case IGR_CHAR_LINE_BREAK:
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
      Result.append(L"\r\n");
#else
      Result.append(L"\n");
#endif
      break;
    case IGR_CHAR_PAGE_BREAK:
    case IGR_CHAR_PARA_BREAK:
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
      Result.append(L"\r\n\r\n");
#else
      Result.append(L"\n\n");
#endif
      break;
    default:
      Result.append(1, Ch);
      break;
    }
  }
  return Result;
}

Metadata Extractor::ParseMetadata(const std::string &S)
{
// ISYS Metadata format:
// Name1: Value1<#14>Name2: Value2<#14>

  Metadata Result;
  bool InName = true;
  std::string Name = "";
  std::string Value = "";
  unsigned int i = 0;
  while (i < S.length())
  {
    char Ch = S[i];
    switch (Ch)
    {
    case ':': // Name & Value separator
    {
      if ((InName) && (i < S.length() - 1) && (S[i + 1] == ' '))
      {
        InName = false;
        i+=2;
      }
      else if (InName)
      {
        Name += Ch;
        i++;
      }
      else
      {
        Value += Ch;
        i++;
      }
      break;
    }
    case IGR_CHAR_PARA_BREAK:
    {
      if ((Name != "") && (Value != ""))
        Result[Name] = Value;
      InName = true;
      Name = "";
      Value = "";
      i++;
      break;
    }
    default:
    {
      if (InName)
        Name += Ch;
      else
        Value += Ch;
      i++;
      break;
    }
    }
  }
  if ((Name != "") && (Value != ""))
    Result[Name] = Value;
  return Result;
}

MetadataW Extractor::ParseMetadataW(const std::wstring &S)
{
// ISYS Metadata format:
// Name1: Value1<#14>Name2: Value2<#14>

  MetadataW Result;
  bool InName = true;
  std::wstring Name = L"";
  std::wstring Value = L"";
  unsigned int i = 0;
  while (i < S.length())
  {
    wchar_t Ch = S[i];
    switch (Ch)
    {
    case ':': // Name & Value separator
    {
      if ((InName) && (i < S.length() - 1) && (S[i + 1] == ' '))
      {
        InName = false;
        i+=2;
      }
      else if (InName)
      {
        Name += Ch;
        i++;
      }
      else
      {
        Value += Ch;
        i++;
      }
      break;
    }
    case IGR_CHAR_PARA_BREAK:
    {
      if ((Name != L"") && (Value != L""))
        Result[Name] = Value;
      InName = true;
      Name = L"";
      Value = L"";
      i++;
      break;
    }
    default:
    {
      if (InName)
        Name += Ch;
      else
        Value += Ch;
      i++;
      break;
    }
    }
  }
  if ((Name != L"") && (Value != L""))
    Result[Name] = Value;
  return Result;
}

SubFile *Extractor::GetFirstSubFile()
{
  return GetNextSubFile();
}

SubFile *Extractor::GetNextSubFile()
{
  Open();
  WCHAR ID[4096];
  WCHAR Name[1024];
  LONGLONG FileTime;
  LONGLONG FileSize;
  ECB_Type ECBError;
  LONG RC = IGR_Get_Subfile_Entry(FDocHandle, ID, Name, &FileTime, &FileSize, &ECBError);
  if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
  {
    std::string Err = "IGR_Get_Subfile_Entry: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  if (RC != IGR_OK)
    return NULL;
  IGR_Stream *IGRStream;
  RC = IGR_Extract_Subfile_Stream(FDocHandle, ID, &IGRStream, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Extract_Subfile_Stream: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  Stream *S = new Stream(IGRStream);
  return new SubFile(S, ToUTF8Str(ID, -1), ToUTF8Str(Name, -1), FileTime, FileSize);
}

SubFile *Extractor::GetSubFile(const std::string &ID)
{
  Open();
  IGR_Stream *IGRStream;
  ECB_Type ECBError;
  LONG RC = IGR_Extract_Subfile_Stream(FDocHandle, _UCS2(ID), &IGRStream, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Extract_Subfile_Stream: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  Stream *S = new Stream(IGRStream);
  return new SubFile(S, ID, "", 0, 0);
}

SubFile *Extractor::GetFirstImage()
{
  return GetNextImage();
}

SubFile *Extractor::GetNextImage()
{
  Open();
  WCHAR ID[4096];
  WCHAR Name[1024];
  LONGLONG FileTime;
  LONGLONG FileSize;
  ECB_Type ECBError;
  LONG RC = IGR_Get_Image_Entry(FDocHandle, ID, Name, &FileTime, &FileSize, &ECBError);
  if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
  {
    std::string Err = "IGR_Get_Image_Entry: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  if (RC != IGR_OK)
    return NULL;
  IGR_Stream *IGRStream;
  RC = IGR_Extract_Image_Stream(FDocHandle, ID, &IGRStream, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Extract_Image_Stream: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  Stream *S = new Stream(IGRStream);
  return new SubFile(S, ToUTF8Str(ID, -1), ToUTF8Str(Name, -1), FileTime, FileSize);
}

FILE *fopen_utf8(const char *filename, const char *mode)
{
  FILE* F;
#if defined(PERCEPTIVE_PLATFORM_WINDOWS)
  // Convert UTF8 filename to wide chars on Windows.
  errno_t fileErr = _wfopen_s(&F, _UCS2(filename), _UCS2(mode));
  if (fileErr) F = NULL;
#else
  F = fopen(filename, mode);
#endif
  return F;
}

void Extractor::CopyTo(const std::string &Filename)
{
  const int FileBufferSize = 4096;

  bool CopyOK = false;
  FILE *F = fopen_utf8(Filename.c_str(), "wb");
  if (F)
  {
    IGR_Stream *S = FStream->getIGRStream();
    LONGLONG StreamPos = S->Seek(S, 0, SEEK_CUR);
    LONGLONG StreamSize = S->Seek(S, 0, SEEK_END);
    S->Seek(S, 0, SEEK_SET);
    LONGLONG BytesWritten = 0;
    void *FileBuffer = malloc(FileBufferSize);
    if (FileBuffer)
    {
      while (BytesWritten < StreamSize)
      {
        ULONG BytesRead = S->Read(S, FileBuffer, FileBufferSize);
        BytesWritten += fwrite(FileBuffer, 1, BytesRead, F);
      }
      free(FileBuffer);
    }
    S->Seek(S, StreamPos, SEEK_SET);
    fclose(F);
    CopyOK = (BytesWritten == StreamSize);
  }
  if (!CopyOK)
    throw Exception("Cannot copy to: " + Filename);
}

void Extractor::SaveTo(const std::string &Filename)
{
  Open();
  bool SaveOK = false;
  FILE *F = fopen_utf8(Filename.c_str(), "wb");
  if (F)
  {
    LONGLONG BytesExtracted = 0;
    LONGLONG BytesWritten = 0;
    while (!getEOF())
    {
      std::string Text = GetText();
      BytesExtracted += Text.length();
      BytesWritten += fwrite(Text.c_str(), 1, Text.length(), F);
    }
    fclose(F);
    SaveOK = (BytesWritten == BytesExtracted);
  }
  if (!SaveOK)
    throw Exception("Cannot save to: " + Filename);
}

std::string Extractor::getHashMD5()
{
  WCHAR strHexOut[PERCEPTIVE_SZ_MD5_HEX_LEN];
  ECB_Type ECBError;
  LONG RC = IGR_Calculate_MD5(FStream->getIGRStream(), strHexOut, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Calculate_MD5: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return ToUTF8Str(strHexOut, PERCEPTIVE_SZ_MD5_HEX_LEN - 1);
}

std::string Extractor::getHashSHA1()
{
  WCHAR strHexOut[PERCEPTIVE_SZ_SHA1_HEX_LEN];
  ECB_Type ECBError;
  LONG RC = IGR_Calculate_SHA1(FStream->getIGRStream(), strHexOut, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Calculate_SHA1: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return ToUTF8Str(strHexOut, PERCEPTIVE_SZ_SHA1_HEX_LEN - 1);
}

void Extractor::Close()
{
  if (FDocHandle > 0)
  {
    ECB_Type ECBError;
    IGR_Close_File(FDocHandle, &ECBError);
    FCapabilities = 0;
    FDocType = 0;
    FDocHandle = 0;
    FTextEOF = false;
    FPageIndex = 0;
  }
}

Page *Extractor::GetFirstPage()
{
  FPageIndex = 0;
  return GetPage(FPageIndex);
}

Page *Extractor::GetNextPage()
{
  FPageIndex++;
  if (FPageIndex < getPageCount())
    return GetPage(FPageIndex);
  return NULL;
}

Page *Extractor::GetPage(const int PageIndex)
{
  Open();
  HPAGE PageHandle;
  ECB_Type ECBError;
  LONG RC = IGR_Open_Page(FDocHandle, PageIndex, &PageHandle, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Open_Page: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  Page *Result = new Page(PageHandle);
  return Result;
}

int Extractor::getPageCount()
{
  Open();
  LONG PageCount;
  ECB_Type ECBError;
  LONG RC = IGR_Get_Page_Count(FDocHandle, &PageCount, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Get_Page_Count: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return PageCount;
}

void Extractor::DumpStatistics(std::ostream &Stream)
{
  double TotalTime = FTimer.getElapsedTimeInMicroSec();
  Stream << "Total time taken: " << std::fixed << std::setprecision(2) << TotalTime / 1000000 << " seconds" << std::endl;
  Stream << "Total number of characters extracted: " << FNumChars << std::endl;
  Stream << "Extraction rate: " << std::fixed << std::setprecision(2) << FNumChars / (TotalTime / 1000000) << " chars/second" << std::endl;
}

/****************************************************************************
* Perceptive::SubFile
****************************************************************************/

SubFile::SubFile(Stream *S, const std::string &ID, const std::string &Name, LONGLONG FileTime, LONGLONG FileSize): Extractor(S)
{
  FID = ID;
  FName = Name;
  FTime = FileTime;
  FSize = FileSize;
}

std::string SubFile::getID()
{
  return FID;
}

std::string SubFile::getName()
{
  return FName;
}

LONGLONG SubFile::getFileDate()
{
  return FTime;
}

LONGLONG SubFile::getFileSize()
{
  return FSize;
}

/****************************************************************************
* Perceptive::Page
****************************************************************************/

Page::Page(HPAGE P)
{
  FPageHandle = P;
  FTextEOF = false;
  FNumChars = 0;
  FWords = NULL;
  FWordIndex = 0;
}

Page::~Page()
{
  Close();
}

void Page::Close()
{
  ECB_Type ECBError;
  IGR_Close_Page(FPageHandle, &ECBError);
  if (FWords)
  {
    free(FWords);
    FWords = NULL;
  }
}

int Page::getWordCount()
{
  LONG WordCount;
  ECB_Type ECBError;
  LONG RC = IGR_Get_Page_Word_Count(FPageHandle, &WordCount, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Get_Page_Word_Count: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return WordCount;
}

int Page::getWidth()
{
  LONG Width;
  LONG Height;
  getDimensions(Width, Height);
  return Width;
}

int Page::getHeight()
{
  LONG Width;
  LONG Height;
  getDimensions(Width, Height);
  return Height;
}

std::string Page::GetText(const int MaxLength)
{
  std::string Result = "";
  ECB_Type ECBError;
  LONG OutBuffSize = MaxLength;
  WCHAR *OutBuffer = (WCHAR *)malloc(MaxLength * sizeof(WCHAR));
  if (OutBuffer)
  {
    LONG RC = IGR_Get_Page_Text(FPageHandle, OutBuffer, &OutBuffSize, &ECBError);
    if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
    {
      free(OutBuffer);
      std::string Err = "IGR_Get_Page_Text: " + GetErrorStr(RC);
      if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
      throw Exception(Err);
    }
    FTextEOF = (RC == IGR_NO_MORE);
    if (OutBuffSize > 0)
    {
      Result = ToUTF8Str(OutBuffer, OutBuffSize);
      FNumChars += OutBuffSize;
    }
    free(OutBuffer);
  }
  return Result;
}

std::wstring Page::GetTextW(const int MaxLength)
{
  std::wstring Result = L"";
  ECB_Type ECBError;
  LONG OutBuffSize = MaxLength;
  WCHAR *OutBuffer = (WCHAR *)malloc(MaxLength * sizeof(WCHAR));
  if (OutBuffer)
  {
    LONG RC = IGR_Get_Page_Text(FPageHandle, OutBuffer, &OutBuffSize, &ECBError);
    if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
    {
      free(OutBuffer);
      std::string Err = "IGR_Get_Page_Text: " + GetErrorStr(RC);
      if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
      throw Exception(Err);
    }
    FTextEOF = (RC == IGR_NO_MORE);
    if (OutBuffSize > 0)
    {
      Result = ToWideStr(OutBuffer, OutBuffSize);
      FNumChars += OutBuffSize;
    }
    free(OutBuffer);
  }
  return Result;
}

Word *Page::GetFirstWord()
{
  if (!FWords)
  {
    LONG WordCount = getWordCount();
    FWords = (IGR_Page_Word *)malloc(WordCount * sizeof(IGR_Page_Word));
    if (FWords)
    {
      ECB_Type ECBError;
      LONG RC = IGR_Get_Page_Words(FPageHandle, 0, &WordCount, FWords, &ECBError);
      if (RC != IGR_OK)
      {
        free(FWords);
        FWords = NULL;
      }
      if (FWords)
        FWordCount = WordCount;
    }
  }
  FWordIndex = 0;
  return GetWord(FWordIndex);
}

Word *Page::GetNextWord()
{
  FWordIndex++;
  return GetWord(FWordIndex);
}

Word *Page::GetWord(const int WordIndex)
{
  if (FWords && (FWordIndex >= 0) && (FWordIndex < FWordCount))
    return new Word(&FWords[FWordIndex], FWordIndex);
  return NULL;
}

SubFile *Page::GetFirstImage()
{
  return GetNextImage();
}

SubFile *Page::GetNextImage()
{
  WCHAR ID[4096];
  WCHAR Name[1024];
  LONGLONG FileTime;
  LONGLONG FileSize;
  ECB_Type ECBError;
  LONG RC = IGR_Get_Page_Image_Entry(FPageHandle, ID, Name, &FileTime, &FileSize, &ECBError);
  if ((RC != IGR_OK) && (RC != IGR_NO_MORE))
  {
    std::string Err = "IGR_Get_Page_Image_Entry: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  if (RC != IGR_OK)
    return NULL;
  IGR_Stream *IGRStream;
  RC = IGR_Extract_Page_Image_Stream(FPageHandle, ID, &IGRStream, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Extract_Page_Image_Stream: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  Stream *S = new Stream(IGRStream);
  return new SubFile(S, ToUTF8Str(ID, -1), ToUTF8Str(Name, -1), FileTime, FileSize);
}

void Page::Redact(int FirstWord, int LastWord)
{
  ECB_Type ECBError;
  LONG RC = IGR_Redact_Page_Text(FPageHandle, FirstWord, LastWord, 0, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Redact_Page_Text: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Page::Redact(Word *FirstWord, Word *LastWord)
{
  Redact(FirstWord->getWordIndex(), LastWord->getWordIndex());
}

void Page::getDimensions(LONG &Width, LONG &Height)
{
  ECB_Type ECBError;
  LONG RC = IGR_Get_Page_Dimensions(FPageHandle, &Width, &Height, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Get_Page_Dimensions: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

/****************************************************************************
* Perceptive::Word
****************************************************************************/

Word::Word(IGR_Page_Word *W, int WordIndex)
{
  FPageWord = W;
  FWordIndex = WordIndex;
}

Word::~Word()
{
}

std::string Word::GetText()
{
  return ToUTF8Str(FPageWord->word, FPageWord->wordLength);
}

std::wstring Word::GetTextW()
{
  return ToWideStr(FPageWord->word, FPageWord->wordLength);
}

int Word::getX()
{
  return FPageWord->x;
}

int Word::getY()
{
  return FPageWord->y;
}

int Word::getWidth()
{
  return FPageWord->width;
}

int Word::getHeight()
{
  return FPageWord->height;
}

int Word::getStyle()
{
  return FPageWord->style;
}

int Word::getCharacterOffset()
{
  return FPageWord->charoffset;
}

int Word::getWordIndex()
{
  return FWordIndex;
}

/****************************************************************************
* Perceptive::Canvas
****************************************************************************/

Canvas::Canvas(HCANVAS C)
{
  FCanvasHandle = C;
}

Canvas::~Canvas()
{
  Close();
}

void Canvas::Close()
{
  ECB_Type ECBError;
  IGR_Close_Canvas(FCanvasHandle, &ECBError);
}

void Canvas::RenderPage(Page* P)
{
  ECB_Type ECBError;
  LONG RC = IGR_Render_Page(P->FPageHandle, FCanvasHandle, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Render_Page: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Arc(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Arc(FCanvasHandle, x, y, x2, y2, x3, y3, x4, y4, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Arc: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::AngleArc(int x, int y, int Radius, int StartAngle, int SweepAngle)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_AngleArc(FCanvasHandle, x, y, Radius, StartAngle, SweepAngle, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_AngleArc: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Chord(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Chord(FCanvasHandle, x, y, x2, y2, x3, y3, x4, y4, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Chord: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Ellipse(int x, int y, int x2, int y2)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Ellipse(FCanvasHandle, x, y, x2, y2, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Ellipse: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Rect(int x, int y, int x2, int y2)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Rect(FCanvasHandle, x, y, x2, y2, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Rect: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::LineTo(int x, int y)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_LineTo(FCanvasHandle, x, y, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_LineTo: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::MoveTo(int x, int y)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_MoveTo(FCanvasHandle, x, y, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_MoveTo: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Pie(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Pie(FCanvasHandle, x, y, x2, y2, x3, y3, x4, y4, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Pie: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::RoundRect(int x, int y, int x2, int y2, int Radius)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_RoundRect(FCanvasHandle, x, y, x2, y2, Radius, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_RoundRect: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::TextOut(int x, int y, const std::string& Text)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_TextOut(FCanvasHandle, x, y, _UCS2(Text), &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_TextOut: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::TextOut(int x, int y, const std::wstring& Text)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_TextOut(FCanvasHandle, x, y, _UCS2(Text), &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_TextOut: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::TextRect(int x, int y, int x2, int y2, const std::string& Text, int Flags)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_TextRect(FCanvasHandle, x, y, x2, y2, _UCS2(Text), Flags, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_TextRect: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::TextRect(int x, int y, int x2, int y2, const std::wstring& Text, int Flags)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_TextRect(FCanvasHandle, x, y, x2, y2, _UCS2(Text), Flags, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_TextRect: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

int Canvas::TextWidth(const std::string& Text)
{
  LONG Width;
  LONG Height;
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_MeasureText(FCanvasHandle, _UCS2(Text), &Width, &Height, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_MeasureText: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return Width;
}

int Canvas::TextHeight(const std::string& Text)
{
  LONG Width;
  LONG Height;
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_MeasureText(FCanvasHandle, _UCS2(Text), &Width, &Height, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_MeasureText: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return Height;
}

int Canvas::TextWidth(const std::wstring& Text)
{
  LONG Width;
  LONG Height;
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_MeasureText(FCanvasHandle, _UCS2(Text), &Width, &Height, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_MeasureText: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return Width;
}

int Canvas::TextHeight(const std::wstring& Text)
{
  LONG Width;
  LONG Height;
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_MeasureText(FCanvasHandle, _UCS2(Text), &Width, &Height, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_MeasureText: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return Height;
}

void Canvas::SetPen(int Color, int Width, int Style)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_SetPen(FCanvasHandle, Color, Width, Style, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_SetPen: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::SetBrush(int Color, int Style)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_SetBrush(FCanvasHandle, Color, Style, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_SetBrush: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::SetFont(const std::string& Name, int Size, int Style)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_SetFont(FCanvasHandle, _UCS2(Name), Size, Style, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_SetFont: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::SetOpacity(BYTE Opacity)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_SetOpacity(FCanvasHandle, Opacity, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_SetOpacity: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::DrawImage(int x, int y, void *ImageData, size_t ImageSize, const std::string &MimeType)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_DrawImage(FCanvasHandle, x, y, ImageData, ImageSize, _UCS2(MimeType), &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_DrawImage: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::DrawScaleImage(int x, int y, int x2, int y2, void *ImageData, size_t ImageSize, const std::string &MimeType)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_DrawScaleImage(FCanvasHandle, x, y, x2, y2, ImageData, ImageSize, _UCS2(MimeType), &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_DrawScaleImage: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Rotation(int Degrees)
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Rotation(FCanvasHandle, Degrees, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Rotation: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

void Canvas::Reset()
{
  ECB_Type ECBError;
  LONG RC = IGR_Canvas_Reset(FCanvasHandle, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Canvas_Reset: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
}

/****************************************************************************
* Perceptive::DocumentFilters
****************************************************************************/

DocumentFilters::DocumentFilters()
{
  FInstance = 0;
}

DocumentFilters::~DocumentFilters()
{
  Uninitialize();
}

void DocumentFilters::Initialize(const std::string &License, const std::string &Path)
{
  if (FInstance == 0)
  {
    ECB_Type ECBError;
    Instance_Status_Block ISB;
    strncpy(ISB.Licensee_ID1, License.c_str(), sizeof(ISB.Licensee_ID1));
    Init_Instance(0, Path.c_str(), &ISB, &FInstance, &ECBError);
    if (strlen(ECBError.Msg) > 0)
      throw Exception("Init_Instance: Unable to initialize Perceptive Document Filters: " + std::string(ECBError.Msg));
  }
}

void DocumentFilters::Uninitialize()
{
  if (FInstance > 0)
  {
    ECB_Type ECBError;
    Close_Instance(&ECBError);
    FInstance = 0;
  }
}

std::string DocumentFilters::GetMemoryStatus()
{
  std::string Result = "";
  long OutBuffSize = 0xFFFF;
  char *OutBuffer = (char *)malloc(OutBuffSize);
  if (OutBuffer)
  {
    size_t RC = IGR_Get_Memory_Status(OutBuffer, OutBuffSize);
    if (RC > 0)
      Result = std::string(OutBuffer);
    free(OutBuffer);
  }
  return Result;
}

Extractor *DocumentFilters::GetExtractor(const std::string &Filename)
{
  if (FInstance == 0)
    throw Exception("Perceptive Document Filters not initialized");
  Stream *S = new FileStream(Filename);
  return GetExtractor(S);
}

Extractor *DocumentFilters::GetExtractor(Stream *S)
{
  if (FInstance == 0)
    throw Exception("Perceptive Document Filters not initialized");
  return new Extractor(S);
}

#define ENC_JAPANESE            "Shift-JIS"
#define ENC_CHINESE_SIMPLIFIED  "GBK"
#define ENC_KOREAN              "ISO-2022-KR"
#define ENC_CHINESE_TRADITIONAL "Big5"
#define ENC_CYRILLIC            "KOI8-R"
#define ENC_LATIN_1             "Latin1"
#define ENC_GREEK               "ISO-8859-7"
#define ENC_TURKISH             "ISO-8859-9"
#define ENC_HEBREW              "Windows-1255"
#define ENC_ARABIC              "ISO-8859-6"
#define ENC_UTF_8               "UTF-8"

void DocumentFilters::SetDefaultInputEncoding(const std::string &Encoding)
{
  int CP = 1252;
  if (SameText(Encoding, ENC_JAPANESE))
    CP = 932;
  else if (SameText(Encoding, ENC_CHINESE_SIMPLIFIED))
    CP = 936;
  else if (SameText(Encoding, ENC_KOREAN))
    CP = 949;
  else if (SameText(Encoding, ENC_CHINESE_TRADITIONAL))
    CP = 950;
  else if (SameText(Encoding, ENC_CYRILLIC))
    CP = 1251;
  else if (SameText(Encoding, ENC_LATIN_1))
    CP = 1252;
  else if (SameText(Encoding, ENC_GREEK))
    CP = 1253;
  else if (SameText(Encoding, ENC_TURKISH))
    CP = 1254;
  else if (SameText(Encoding, ENC_HEBREW))
    CP = 1255;
  else if (SameText(Encoding, ENC_ARABIC))
    CP = 1256;
  else if (SameText(Encoding, ENC_UTF_8))
    CP = 65001;
  SetDefaultInputCodePage(CP);
}

void DocumentFilters::SetDefaultInputCodePage(const int CP)
{
  size_t L1 = CP;
  size_t L2 = 0;
  ECB_Type ECBError;
  IGR_Multiplex(IGR_Multi_Set_Code_Page, &L1, &L2, &ECBError);
}

void DocumentFilters::SetTempPath(const std::string &Path)
{
  size_t L1 = (size_t)Path.c_str();
  size_t L2 = 0;
  ECB_Type ECBError;
  IGR_Multiplex(IGR_Multi_Set_Temp_Path, &L1, &L2, &ECBError);
}

void DocumentFilters::DumpStatistics(std::ostream &S)
{
  // This functionality has been moved to Extractor::DumpStatistics()
}

Canvas* DocumentFilters::MakeOutputCanvas(const std::string& Filename, int Type, const std::string& Options)
{
  HCANVAS CanvasHandle;
  ECB_Type ECBError;
  LONG RC = IGR_Make_Output_Canvas(Type, _UCS2(Filename), _UCS2(Options), &CanvasHandle, &ECBError);
  if (RC != IGR_OK)
  {
    std::string Err = "IGR_Make_Output_Canvas: " + GetErrorStr(RC);
    if (strlen(ECBError.Msg) > 0) Err += ", " + std::string(ECBError.Msg);
    throw Exception(Err);
  }
  return new Canvas(CanvasHandle);
}

/****************************************************************************
* Utility functions
****************************************************************************/

std::string GetErrorStr(const int ReturnCode)
{
  std::string Result = "Unknown";
  switch (ReturnCode)
  {
  case IGR_OK:
    Result = "OK";
    break;
  case IGR_E_OPEN_ERROR:
    Result = "Document open error";
    break;
  case IGR_E_WRONG_TYPE:
    Result = "Document is wrong type";
    break;
  case IGR_E_IN_USE:
    Result = "Document is in use";
    break;
  case IGR_E_NOT_READABLE:
    Result = "Document is not readable";
    break;
  case IGR_E_PASSWORD:
    Result = "Document is password protected";
    break;
  case IGR_E_NOT_FOUND:
    Result = "Document not found";
    break;
  case IGR_E_WRITE_ERROR:
    Result = "Document write error";
    break;
  case IGR_E_NOT_VALID_FOR_THIS_CLASS:
    Result = "Document operation not valid";
    break;
  case IGR_E_ERROR:
    Result = "Document error";
    break;
  case IGR_E_INVALID_HANDLE:
    Result = "Invalid document handle";
    break;
  case IGR_E_INVALID_POINTER:
    Result = "Invalid pointer";
    break;
  case IGR_E_INVALID_PARAMETER:
    Result = "Invalid parameter";
    break;
  case IGR_E_FILE_CORRUPT:
    Result = "Document is unreadable or corrupt";
    break;
  case IGR_E_OUT_OF_MEMORY:
    Result = "Out of memory";
    break;
  case IGR_E_BAD_ERROR:
    Result = "Unknown exception";
    break;
  case IGR_E_WRONG_THREAD:
    Result = "Document belongs to different thread";
    break;
  case IGR_E_TOO_MANY_HANDLES:
    Result = "Too many documents open";
    break;
  }
  return Result;
}

std::string GetFileTypeName(const int FileType)
{
  const int BufferLen = 256;
  std::string Result = "Unknown";

  ECB_Type ECBError;
  LPSTR Buffer = (LPSTR)malloc(BufferLen);
  if (Buffer)
  {
    LONG RC = IGR_Get_Format_Attribute(FileType, 0, Buffer, &ECBError);
    if (RC == IGR_OK)
      Result.assign((char *)Buffer);
    free(Buffer);
  }
  return Result;
}

std::string HTMLEncode(const std::string &S)
{
  std::string Result;
  Result.reserve(S.length() * 2);
  for (size_t i = 0; i < S.length(); i++)
  {
    char Ch = S[i];
    switch (Ch)
    {
    case '<':
      Result.append("&lt;");
      break;
    case '>':
      Result.append("&gt;");
      break;
    case '&':
      Result.append("&amp;");
      break;
    case '"':
      Result.append("&quot;");
      break;
    default:
      //if (Ch >= 0x80)
      //  Result.append("&#" + itoa(Ch));
      //else
      Result.append(1, Ch);
      break;
    }
  }
  return Result;
}

std::string ReplaceAll(const std::string &S, const std::string& Search, const std::string& Replace)
{
  std::string Result = S;
  size_t i = 0;
  while (true)
  {
    i = Result.find(Search, i);
    if (i == std::string::npos) break;
    Result.replace(i, Search.length(), Replace);
    i += Replace.length();
  }
  return Result;
}

void PrepareFileForWideOutput(FILE *F, const bool IncludeBOM)
{
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
  _setmode(_fileno(F), O_BINARY);
#endif
  if (IncludeBOM)
  {
#if defined(PERCEPTIVE_PLATFORM_LITTLE_ENDIAN)
    fwrite("\xFF\xFE", 1, strlen("\xFF\xFE"), F);
#elif defined(PERCEPTIVE_PLATFORM_BIG_ENDIAN)
    fwrite("\xFF\xFE", 1, strlen("\xFE\xFF"), F);
#endif
  }
}

void PrepareFileForUTF8Output(FILE *F, const bool IncludeBOM)
{
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
  _setmode(_fileno(F), O_BINARY);
#endif
  if (IncludeBOM)
    fwrite("\xEF\xBB\xBF", 1, strlen("\xEF\xBB\xBF"), F);
}

std::string ReadTextFile(const std::string &Filename)
{
  std::string Result = "";
#if defined(PERCEPTIVE_PLATFORM_WINDOWS)
  // Convert UTF8 filename to wide chars on Windows.
  FILE* F = 0;
  std::string wFilename = ToWCHARStr(Filename);
  errno_t fileErr = _wfopen_s(&F, (wchar_t*)wFilename.c_str(), L"r");
  if (!fileErr)
#else
  FILE *F = fopen(Filename.c_str(), "r");
  if (F)
#endif
  {
    while (!feof(F))
    {
      char LineStr[256];
      (void)fgets(LineStr, sizeof(LineStr), F);
      Result += LineStr;
    }
    fclose(F);
  }
  return Result;
}

//////////

bool SameText(const std::string &S1, const std::string &S2)
{
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
  return _stricmp(S1.c_str(), S2.c_str()) == 0;
#else
  return strcasecmp(S1.c_str(), S2.c_str()) == 0;
#endif
}

std::string IncludeTrailingPathDelimiter(const std::string &Path)
{
  std::string Result = Path;
  if ((Result.length() > 0) && (Result[Result.length() - 1] != PathDelim))
    Result += PathDelim;
  return Result;
}

std::string RemoveTrailingPathDelimiter(const std::string &Path)
{
  std::string Result = Path;
  if ((Result.length() > 0) && (Result[Result.length() - 1] == PathDelim))
    Result.erase(Result.length() - 1, 1);
  return Result;
}

std::string JoinFilename(const std::string &Path, const std::string &Filename)
{
  return IncludeTrailingPathDelimiter(Path) + Filename;
}

std::string MakeSafeFilename(const std::string &Filename)
{
  std::string Result = "";
  for (size_t i = 0; i < Filename.length(); i++)
    if ((Filename[i] < ' ') || (Filename[i] == '<') || (Filename[i] == '>') || (Filename[i] == ':') ||
        (Filename[i] == '"') || (Filename[i] == '|') || (Filename[i] == '?') || (Filename[i] == '*'))
      Result += '_';
    else if ((Filename[i] == '\\') || (Filename[i] == '/'))
      Result += PathDelim;
    else
      Result += Filename[i];
  return Result;
}

bool FileExists(const std::string &Filename)
{
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
  struct _stat buf;
  return(_stat(Filename.c_str(), &buf) == 0);
#else
  struct stat buf;
  return(stat(Filename.c_str(), &buf) == 0);
#endif
}

bool DirExists(const std::string &Path)
{
  std::string PathEx = RemoveTrailingPathDelimiter(Path);
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
  struct _stat buf;
  return((_stat(PathEx.c_str(), &buf) == 0) && (buf.st_mode & _S_IFDIR));
#else
  struct stat buf;
  return((stat(PathEx.c_str(), &buf) == 0) && (buf.st_mode & S_IFDIR));
#endif
}

bool CreateFolder(const std::string &Path)
{
  bool Result = DirExists(Path);
  if (!Result)
  {
    size_t Found = Path.find_last_of(PathDelim);
    if (Found != std::string::npos)
    {
      std::string ParentPath = Path.substr(0, Found);
      if (!DirExists(ParentPath))
        Result = CreateFolder(ParentPath);
    }
    if (!DirExists(Path))
#ifdef PERCEPTIVE_PLATFORM_WINDOWS
      Result = (_mkdir(Path.c_str()) == 0);
#else
      Result = (mkdir(Path.c_str(), 0755) == 0);
#endif
  }
  return(Result);
}

}; // namespace Perceptive
