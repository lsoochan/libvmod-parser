/*
 * NB:  This file is machine generated, DO NOT EDIT!
 *
 * Edit vmod.vcc and run vmod.py instead
 */

#include "vrt.h"
#include "vcc_if.h"
#include "vmod_abi.h"


typedef void td_parsereq_init(struct sess *);
typedef int td_parsereq_errcode(struct sess *);
typedef void td_parsereq_debuginit(struct sess *);
typedef void td_parsereq_setopt(struct sess *, const char *);
typedef const char * td_parsereq_param(struct sess *, const char *, const char *);
typedef int td_parsereq_size(struct sess *, const char *, const char *);
typedef const char * td_parsereq_body(struct sess *, const char *);
typedef const char * td_parsereq_next_key(struct sess *, const char *);
typedef void td_parsereq_next_offset(struct sess *, const char *);
typedef const char * td_parsereq_current_key(struct sess *, const char *);
typedef unsigned td_parsereq_iterate(struct sess *, const char *, const char *);
typedef void td_parsereq_reset_offset(struct sess *, const char *);
typedef const char * td_parsereq_post_header(struct sess *, const char *);
typedef const char * td_parsereq_get_header(struct sess *, const char *);
typedef const char * td_parsereq_cookie_header(struct sess *, const char *);
typedef const char * td_parsereq_post_body(struct sess *);
typedef const char * td_parsereq_get_body(struct sess *);
typedef const char * td_parsereq_cookie_body(struct sess *);
typedef const char * td_parsereq_post_read_keylist(struct sess *);
typedef const char * td_parsereq_get_read_keylist(struct sess *);
typedef const char * td_parsereq_cookie_read_keylist(struct sess *);
typedef void td_parsereq_post_seek_reset(struct sess *);
typedef void td_parsereq_get_seek_reset(struct sess *);
typedef void td_parsereq_cookie_seek_reset(struct sess *);

const char Vmod_Name[] = "parsereq";
const struct Vmod_Func_parsereq {
	td_parsereq_init	*init;
	td_parsereq_errcode	*errcode;
	td_parsereq_debuginit	*debuginit;
	td_parsereq_setopt	*setopt;
	td_parsereq_param	*param;
	td_parsereq_size	*size;
	td_parsereq_body	*body;
	td_parsereq_next_key	*next_key;
	td_parsereq_next_offset	*next_offset;
	td_parsereq_current_key	*current_key;
	td_parsereq_iterate	*iterate;
	td_parsereq_reset_offset	*reset_offset;
	td_parsereq_post_header	*post_header;
	td_parsereq_get_header	*get_header;
	td_parsereq_cookie_header	*cookie_header;
	td_parsereq_post_body	*post_body;
	td_parsereq_get_body	*get_body;
	td_parsereq_cookie_body	*cookie_body;
	td_parsereq_post_read_keylist	*post_read_keylist;
	td_parsereq_get_read_keylist	*get_read_keylist;
	td_parsereq_cookie_read_keylist	*cookie_read_keylist;
	td_parsereq_post_seek_reset	*post_seek_reset;
	td_parsereq_get_seek_reset	*get_seek_reset;
	td_parsereq_cookie_seek_reset	*cookie_seek_reset;
	vmod_init_f	*_init;
} Vmod_Func = {
	vmod_init,
	vmod_errcode,
	vmod_debuginit,
	vmod_setopt,
	vmod_param,
	vmod_size,
	vmod_body,
	vmod_next_key,
	vmod_next_offset,
	vmod_current_key,
	vmod_iterate,
	vmod_reset_offset,
	vmod_post_header,
	vmod_get_header,
	vmod_cookie_header,
	vmod_post_body,
	vmod_get_body,
	vmod_cookie_body,
	vmod_post_read_keylist,
	vmod_get_read_keylist,
	vmod_cookie_read_keylist,
	vmod_post_seek_reset,
	vmod_get_seek_reset,
	vmod_cookie_seek_reset,
	init_function,
};

const int Vmod_Len = sizeof(Vmod_Func);

const char Vmod_Proto[] =
	"typedef void td_parsereq_init(struct sess *);\n"
	"typedef int td_parsereq_errcode(struct sess *);\n"
	"typedef void td_parsereq_debuginit(struct sess *);\n"
	"typedef void td_parsereq_setopt(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_param(struct sess *, const char *, const char *);\n"
	"typedef int td_parsereq_size(struct sess *, const char *, const char *);\n"
	"typedef const char * td_parsereq_body(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_next_key(struct sess *, const char *);\n"
	"typedef void td_parsereq_next_offset(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_current_key(struct sess *, const char *);\n"
	"typedef unsigned td_parsereq_iterate(struct sess *, const char *, const char *);\n"
	"typedef void td_parsereq_reset_offset(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_post_header(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_get_header(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_cookie_header(struct sess *, const char *);\n"
	"typedef const char * td_parsereq_post_body(struct sess *);\n"
	"typedef const char * td_parsereq_get_body(struct sess *);\n"
	"typedef const char * td_parsereq_cookie_body(struct sess *);\n"
	"typedef const char * td_parsereq_post_read_keylist(struct sess *);\n"
	"typedef const char * td_parsereq_get_read_keylist(struct sess *);\n"
	"typedef const char * td_parsereq_cookie_read_keylist(struct sess *);\n"
	"typedef void td_parsereq_post_seek_reset(struct sess *);\n"
	"typedef void td_parsereq_get_seek_reset(struct sess *);\n"
	"typedef void td_parsereq_cookie_seek_reset(struct sess *);\n"
	"\n"
	"struct Vmod_Func_parsereq {\n"
	"	td_parsereq_init	*init;\n"
	"	td_parsereq_errcode	*errcode;\n"
	"	td_parsereq_debuginit	*debuginit;\n"
	"	td_parsereq_setopt	*setopt;\n"
	"	td_parsereq_param	*param;\n"
	"	td_parsereq_size	*size;\n"
	"	td_parsereq_body	*body;\n"
	"	td_parsereq_next_key	*next_key;\n"
	"	td_parsereq_next_offset	*next_offset;\n"
	"	td_parsereq_current_key	*current_key;\n"
	"	td_parsereq_iterate	*iterate;\n"
	"	td_parsereq_reset_offset	*reset_offset;\n"
	"	td_parsereq_post_header	*post_header;\n"
	"	td_parsereq_get_header	*get_header;\n"
	"	td_parsereq_cookie_header	*cookie_header;\n"
	"	td_parsereq_post_body	*post_body;\n"
	"	td_parsereq_get_body	*get_body;\n"
	"	td_parsereq_cookie_body	*cookie_body;\n"
	"	td_parsereq_post_read_keylist	*post_read_keylist;\n"
	"	td_parsereq_get_read_keylist	*get_read_keylist;\n"
	"	td_parsereq_cookie_read_keylist	*cookie_read_keylist;\n"
	"	td_parsereq_post_seek_reset	*post_seek_reset;\n"
	"	td_parsereq_get_seek_reset	*get_seek_reset;\n"
	"	td_parsereq_cookie_seek_reset	*cookie_seek_reset;\n"
	"	vmod_init_f	*_init;\n"
	"} Vmod_Func_parsereq;\n"
	;

const char * const Vmod_Spec[] = {
	"parsereq.init\0Vmod_Func_parsereq.init\0VOID\0",
	"parsereq.errcode\0Vmod_Func_parsereq.errcode\0INT\0",
	"parsereq.debuginit\0Vmod_Func_parsereq.debuginit\0VOID\0",
	"parsereq.setopt\0Vmod_Func_parsereq.setopt\0VOID\0ENUM\0enable_post_lookup\0\0",
	"parsereq.param\0Vmod_Func_parsereq.param\0STRING\0ENUM\0post\0get\0cookie\0req\0auto\0\0STRING\0",
	"parsereq.size\0Vmod_Func_parsereq.size\0INT\0ENUM\0post\0get\0cookie\0req\0auto\0\0STRING\0",
	"parsereq.body\0Vmod_Func_parsereq.body\0STRING\0ENUM\0post\0get\0cookie\0\0",
	"parsereq.next_key\0Vmod_Func_parsereq.next_key\0STRING\0ENUM\0post\0get\0cookie\0req\0auto\0\0",
	"parsereq.next_offset\0Vmod_Func_parsereq.next_offset\0VOID\0ENUM\0post\0get\0cookie\0req\0auto\0\0",
	"parsereq.current_key\0Vmod_Func_parsereq.current_key\0STRING\0ENUM\0post\0get\0cookie\0req\0auto\0\0",
	"parsereq.iterate\0Vmod_Func_parsereq.iterate\0BOOL\0ENUM\0post\0get\0cookie\0req\0\0STRING\0",
	"parsereq.reset_offset\0Vmod_Func_parsereq.reset_offset\0VOID\0ENUM\0post\0get\0cookie\0req\0auto\0\0",
	"parsereq.post_header\0Vmod_Func_parsereq.post_header\0STRING\0STRING\0",
	"parsereq.get_header\0Vmod_Func_parsereq.get_header\0STRING\0STRING\0",
	"parsereq.cookie_header\0Vmod_Func_parsereq.cookie_header\0STRING\0STRING\0",
	"parsereq.post_body\0Vmod_Func_parsereq.post_body\0STRING\0",
	"parsereq.get_body\0Vmod_Func_parsereq.get_body\0STRING\0",
	"parsereq.cookie_body\0Vmod_Func_parsereq.cookie_body\0STRING\0",
	"parsereq.post_read_keylist\0Vmod_Func_parsereq.post_read_keylist\0STRING\0",
	"parsereq.get_read_keylist\0Vmod_Func_parsereq.get_read_keylist\0STRING\0",
	"parsereq.cookie_read_keylist\0Vmod_Func_parsereq.cookie_read_keylist\0STRING\0",
	"parsereq.post_seek_reset\0Vmod_Func_parsereq.post_seek_reset\0VOID\0",
	"parsereq.get_seek_reset\0Vmod_Func_parsereq.get_seek_reset\0VOID\0",
	"parsereq.cookie_seek_reset\0Vmod_Func_parsereq.cookie_seek_reset\0VOID\0",
	"INIT\0Vmod_Func_parsereq._init",
	0
};
const char Vmod_Varnish_ABI[] = VMOD_ABI_Version;
const void * const Vmod_Id = &Vmod_Id;

