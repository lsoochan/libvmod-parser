/*
 * NB:  This file is machine generated, DO NOT EDIT!
 *
 * Edit vmod.vcc and run vmod.py instead
 */

struct sess;
struct VCL_conf;
struct vmod_priv;

void vmod_init(struct sess *);
int vmod_errcode(struct sess *);
void vmod_debuginit(struct sess *);
void vmod_setopt(struct sess *, const char *);
const char * vmod_param(struct sess *, const char *, const char *);
int vmod_size(struct sess *, const char *, const char *);
const char * vmod_body(struct sess *, const char *);
const char * vmod_next_key(struct sess *, const char *);
void vmod_next_offset(struct sess *, const char *);
const char * vmod_current_key(struct sess *, const char *);
unsigned vmod_iterate(struct sess *, const char *, const char *);
void vmod_reset_offset(struct sess *, const char *);
const char * vmod_post_header(struct sess *, const char *);
const char * vmod_get_header(struct sess *, const char *);
const char * vmod_cookie_header(struct sess *, const char *);
const char * vmod_post_body(struct sess *);
const char * vmod_get_body(struct sess *);
const char * vmod_cookie_body(struct sess *);
const char * vmod_post_read_keylist(struct sess *);
const char * vmod_get_read_keylist(struct sess *);
const char * vmod_cookie_read_keylist(struct sess *);
void vmod_post_seek_reset(struct sess *);
void vmod_get_seek_reset(struct sess *);
void vmod_cookie_seek_reset(struct sess *);
int init_function(struct vmod_priv *, const struct VCL_conf *);
extern const void * const Vmod_Id;
