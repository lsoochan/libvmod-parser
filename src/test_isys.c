#include <stdio.h>

#include "PerceptiveDocumentFilters.h"
#include "PerceptiveDocumentFiltersLicense.inc"
#include <ctype.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>

#define MAX_OPTION 512
#define FILTERING_BUFFER_SIZE	1024 * 1024 * 30
#define FILTERING_WBUFFER_SIZE	FILTERING_BUFFER_SIZE * 2

extern size_t parse_hancell(const char *src, size_t src_size, char *dst, size_t dst_size);
extern const char *parse_hancell_file(const char *path);

static size_t str_len__filtering_buffer;
static u_char filtering_buffer[FILTERING_BUFFER_SIZE + 1];

static size_t str_len__filtering_wbuffer;
static wchar_t filtering_wbuffer[FILTERING_BUFFER_SIZE + 1];


static wchar_t ISYS_woption[MAX_OPTION*2 + 1];
static wchar_t ISYS_ID[4096];
static wchar_t ISYS_Name[1024];
static char ISYS_option[MAX_OPTION];
static wchar_t FILE_NAME[1024];
static char fname[512];

void init_filtering(void)
{
	ECB_Type ISYSError;
	SHORT ISYSHandle;
	Instance_Status_Block ISB;
	size_t L1 = 949;
	size_t L2 = 0;
strcpy(ISYS_option, "EXCELMODE=CSV;STYLESHEET=Off;IMAGES=No;SHOWHIDDEN=Visible;HDISOLATIONMAXTIME=2;PDFXMPMETA=Off;PDFBOOKMARKS=off;PDFANNOTATIONS=Off;PDFIMAGES=No;PDFMAXPROCESSINGTIME=5;PDFTHUMBNAILS=No;");
//strcpy(ISYS_option, "WATERMARK=test");
	strncpy(ISB.Licensee_ID1, LICENSE_KEY, sizeof(ISB.Licensee_ID1));
	Init_Instance(0, ".", &ISB, &ISYSHandle, &ISYSError);
	UTF8_to_Widechar(ISYS_option, ISYS_woption, 1024);
	if(strlen(ISYSError.Msg) > 0) {
		printf("Init_Instance : Unable to initialize ISYS Document Filters : %s\n",ISYSError.Msg);
		exit(1);
	}
	IGR_Multiplex(IGR_Multi_Set_Code_Page, &L1, &L2, &ISYSError);

	strcpy(fname, "test.pdf");
	UTF8_to_Widechar(fname, FILE_NAME, 1024);
}


LONG ISYS_extract_subfile(LONG DocHandle, LONG m_sub_size_plus)
{
	printf("extract subfile start\n");
	struct IGR_Stream *subStream = NULL;
	LONG RC3;
	LONG sub_size = FILTERING_WBUFFER_SIZE;
	LONG sub_size_plus = m_sub_size_plus;
	ECB_Type ISYSError;
	LONG size = FILTERING_WBUFFER_SIZE;
	u_int64_t FileDate, FileSize;
	while(1) {
		LONG subDocHandle,sub_cp, sub_dt;
		RC3 = IGR_Get_Subfile_Entry(DocHandle, ISYS_ID, ISYS_Name,&FileDate, &FileSize, &ISYSError);
		if(RC3 != IGR_OK) {
			printf("no subfile\n");
			break;
		}
		RC3 = IGR_Extract_Subfile_Stream(DocHandle, ISYS_ID, &subStream, &ISYSError);
		if(RC3 == IGR_OK) {
			printf("hihi\n");
			RC3 = IGR_Open_Stream_Ex(subStream, IGR_FORMAT_HTML, ISYS_woption, &sub_cp, &sub_dt, &subDocHandle, &ISYSError);
			printf("hihihihi\n");
			if(sub_dt == 52) {
			}
			if(RC3 != IGR_OK) {
				continue;
			}
			printf("LSC - sub cp = %d\nsub dt = %d\n", sub_cp, sub_dt);
			if((sub_cp & 2) || (sub_dt == 56))
				sub_size_plus = ISYS_extract_subfile(subDocHandle, sub_size_plus);
			RC3 = IGR_Get_Text(subDocHandle, filtering_wbuffer, &sub_size, &ISYSError);
			sub_size = FILTERING_WBUFFER_SIZE;
			if(RC3 != IGR_OK) {
				continue;
			}
			Widechar_to_UTF8(filtering_wbuffer, filtering_buffer+sub_size_plus, FILTERING_BUFFER_SIZE-sub_size_plus);
			sub_size_plus = strlen(filtering_buffer);
		}else
			printf("IGR_Extract_Subfile_Stream error\n");
	}
	if(subStream != NULL)
		subStream->Close(subStream);
	return sub_size_plus;
}

int ISYS_extract_text_on_mm(char *src)
{
        wchar_t wPATH[1024+1];
        LONG DocHandle, cp, dt;
        LONG RC1, RC2, RC3;
        LONG size = FILTERING_WBUFFER_SIZE;
        LONG sub_size_plus = 0;
        struct IGR_Stream *pStream = NULL;
        ECB_Type ISYSError;
        printf("extract text start\n");
        UTF8_to_Widechar(src, wPATH, 1024);
        printf("mk stream from file start\n");
        RC1 = IGR_Make_Stream_From_File(wPATH, 0, &pStream, &ISYSError);
        if (RC1 != IGR_OK) {
                printf("mk stream failed\n");
                return -1;
        }
        printf("open stream ex start\n");
        RC2 = IGR_Open_Stream_Ex(pStream, IGR_FORMAT_HTML, ISYS_woption, &cp, &dt, &DocHandle, &ISYSError);
        printf("open stream ex end\n");
        if (RC2 != IGR_OK) {
                printf("open stream ex failed code : %d doc type : %d\n", RC2, dt);
                str_len__filtering_buffer = strlen(filtering_buffer);
                str_len__filtering_wbuffer = mbstowcs( filtering_wbuffer, filtering_buffer, FILTERING_WBUFFER_SIZE );
                pStream->Close(pStream);
                return 1;
        }
        memset(filtering_buffer, '\0', FILTERING_BUFFER_SIZE*sizeof(char));
        printf("LSC - cp = %d\ndt = %d\n", cp, dt);

	if (dt == 52 || dt == 50) {
		//parse_hancell(const char *src, size_t src_size, char *dst, size_t dst_size)
		char *test_buf = parse_hancell_file(src);
		printf("hancell!! : %d\n%s\n",strlen(test_buf), test_buf);
		//free(test_buf);
		return 1;
	}
	if(1) {           // need to check subfile
                sub_size_plus = ISYS_extract_subfile(DocHandle, 0);
                RC3 = IGR_Get_Text(DocHandle, filtering_wbuffer, &size, &ISYSError);
                if(RC3 == IGR_OK) {
                        str_len__filtering_wbuffer = size;
                        Widechar_to_UTF8(filtering_wbuffer, filtering_buffer+sub_size_plus, FILTERING_BUFFER_SIZE-sub_size_plus);
                        sub_size_plus = strlen(filtering_buffer);
                }
        }else {                 // it is only text
                RC3 = IGR_Get_Text(DocHandle, filtering_wbuffer, &size, &ISYSError);
                if(RC3 == IGR_OK) {
                        str_len__filtering_wbuffer = size;
                        Widechar_to_UTF8(filtering_wbuffer, filtering_buffer, FILTERING_BUFFER_SIZE);
                }
        }
        str_len__filtering_buffer = strlen(filtering_buffer);
        str_len__filtering_wbuffer = mbstowcs( filtering_wbuffer, filtering_buffer, FILTERING_WBUFFER_SIZE );
        pStream->Close(pStream);
        printf("LSC - filtering buffer : \n %s\n", filtering_buffer);
        return 1;
}


int main(int argc, char *argv[]) {
	init_filtering();
	if(2 > argc) {
		printf("usage : ./test_isys <file path> \n");	
		exit(0);
	}
	ISYS_extract_text_on_mm(argv[1]);
	return 0;
}
